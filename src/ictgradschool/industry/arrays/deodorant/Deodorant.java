package ictgradschool.industry.arrays.deodorant;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    @Override
    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info +=  " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() {
        return this.price;
    }

    public String getBrand() {
        return this.brand;
    }

    public boolean isRollOn() {
        return this.rollOn;
    }

    public String getFragrance() {
        return this.fragrance;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setFragrance(String fragrance) {
        this.fragrance = fragrance;
    }

    public boolean isMoreExpensiveThan(Deodorant other) {
        return this.price > other.price;
    }

    public static void main(String[] args) {
        Deodorant myDeodorant = new Deodorant("Gentle", "Baby Powder", true, 4.99);
        Deodorant yourDeodorant = new Deodorant("Spring", "Blossom", false, 3.99);

        System.out.println("1. " + myDeodorant);
        myDeodorant.setBrand("Sweet");
        yourDeodorant.setPrice(5.29);
        System.out.println("2. " + yourDeodorant);

        if (myDeodorant.isRollOn()) {
            System.out.println("3. Roll On");
        } else {
            System.out.println("3. Spray");
        }

        System.out.println("4. " + myDeodorant.toString());

        if (yourDeodorant.isMoreExpensiveThan(myDeodorant)) {
            System.out.println("5. Most expensive is " + yourDeodorant.getBrand());
        } else {
            System.out.println("5. Most expensive is " + myDeodorant.getBrand());
        }
    }

}