package ictgradschool.industry.arrays.lecturers;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {
        // TODO Complete this constructor method
        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }
    
    // TODO Insert getName() method here
    public String getName() {
        return this.name;
    }
    
    // TODO Insert setName() method here
    public void setName(String name) {
        this.name = name;
    }
    
    // TODO Insert getStaffId() method here
    public int getStaffId() {
        return this.staffId;
    }
    
    // TODO Insert setStaffId() method here
    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }
    
    // TODO Insert getPapers() method here
    public String[] getPapers() {
        return this.papers;
    }
    
    // TODO Insert setPapers() method here
    public void setPapers(String[] papers) {
        this.papers = papers;
    }
    
    // TODO Insert isOnLeave() method here
    public boolean isOnLeave() {
        return this.onLeave;
    }
    
    // TODO Insert setOnLeave() method here
    public void setOnLeave(boolean onLeave) {
        this.onLeave = onLeave;
    }

    // TODO Insert toString() method here
    @Override
    public String toString() {
        return "id: " + this.staffId + " "
                + this.name + " is teaching "
                + this.papers.length + " papers.";
    }
    
    // TODO Insert teachesMorePapersThan() method here
    public boolean teachesMorePapersThan(Lecturer other) {
        return this.papers.length > other.papers.length;
    }

}


