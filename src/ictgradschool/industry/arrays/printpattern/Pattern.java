package ictgradschool.industry.arrays.printpattern;

public class Pattern {

    private char[] charArray;
    private char c;

    public Pattern(int size, char c) {
        createArray(size, c);
    }

    @Override
    public String toString() {
        return String.valueOf(this.charArray);
    }

    public int getNumberOfCharacters() {
        return this.charArray.length;
    }

    public void setNumberOfCharacters(int newSize) {
        createArray(newSize, this.c);
    }

    private void createArray(int size, char c) {
        this.c = c;
        this.charArray = new char[size];
        for (int i = 0; i < this.charArray.length; i++) {
            this.charArray[i] = this.c;
        }
    }
}
